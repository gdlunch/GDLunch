FROM adoptopenjdk:11-jre-hotspot

MAINTAINER labuda.dev

COPY build/libs/GDLunch-1.0-SNAPSHOT.jar gdlunch.jar
COPY restaurants.json restaurants.json
COPY apiKeys.properties apiKeys.properties

EXPOSE 8080

ENTRYPOINT java -Xms32m -Xmx128m -Duser.timezone=Europe/Prague -jar gdlunch.jar
