package dev.labuda.gdlunch.util

import com.labuda.gdlunch.util.PropertiesConfig
import mu.KotlinLogging
import java.io.IOException
import kotlin.system.exitProcess

object ApiKeys : PropertiesConfig() {
    val logger = KotlinLogging.logger { }

    init {
        try {
            load("apiKeys.properties")
        } catch (e: IOException) {
            logger.error(e) { "ApiKeys could not be initialized" }
            exitProcess(-1)
        }
    }
}
