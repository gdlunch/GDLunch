package dev.labuda.gdlunch.parser

import com.labuda.gdlunch.parser.AbstractRestaurantWebParser
import com.labuda.gdlunch.parser.DailyParser
import com.labuda.gdlunch.repository.entity.DailyMenu
import com.labuda.gdlunch.repository.entity.MenuItem
import com.labuda.gdlunch.repository.entity.Restaurant
import org.jsoup.Jsoup
import java.time.LocalDate

open class MenickaParser(restaurant: Restaurant) : AbstractRestaurantWebParser(restaurant), DailyParser {

    override fun parse(): DailyMenu =
            DailyMenu(
                    LocalDate.now(),
                    restaurant,
                    Jsoup.connect(restaurant.parserUrl)
                            .get()
                            .selectFirst("div.menicka ul")
                            .select("li")
                            .map {
                                MenuItem(
                                        it.select("div.polozka").text().trim(),
                                        parsePrice(it.select("div.cena").text().trim())
                                )
                            }
            )
}
