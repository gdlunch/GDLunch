package dev.labuda.gdlunch.parser

import com.labuda.gdlunch.parser.AbstractRestaurantWebParser
import com.labuda.gdlunch.parser.DailyParser
import com.labuda.gdlunch.repository.entity.DailyMenu
import com.labuda.gdlunch.repository.entity.MenuItem
import com.labuda.gdlunch.repository.entity.Restaurant
import mu.KotlinLogging
import org.jsoup.Jsoup
import java.time.LocalDate

class JeanPaulsParser(restaurant: Restaurant) : AbstractRestaurantWebParser(restaurant), DailyParser {

    val logger = KotlinLogging.logger { }

    override fun parse(): DailyMenu = DailyMenu(
            LocalDate.now(),
            restaurant,
            Jsoup.connect(restaurant.parserUrl)
                    .get()
                    .selectFirst("div.denni-menu table")
                    .select("tr")
                    .map {
                        MenuItem(
                                it.select("div.text").text().trim(),
                                parsePrice(it.select("div.price").text().trim())
                        )
                    }
    )


}
