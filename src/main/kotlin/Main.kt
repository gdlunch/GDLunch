package dev.labuda.gdlunch

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableScheduling

@EnableJpaRepositories("com.labuda.gdlunch")
@EntityScan("com.labuda.gdlunch")
@EnableScheduling
@SpringBootApplication(scanBasePackages = ["com.labuda.gdlunch", "dev.labuda.gdlunch"])
class Main

fun main(args: Array<String>) {
    runApplication<Main>(*args)
}

