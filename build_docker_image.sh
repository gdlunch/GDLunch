#!/usr/bin/env bash

set -e

./gradlew clean build \
  && docker buildx build --platform linux/arm/v7 -t labuda/gdlunch:latest . \
  && docker push labuda/gdlunch:latest
